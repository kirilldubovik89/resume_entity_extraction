from random import seed

from yargy import Parser, and_, or_, rule
from yargy.interpretation import fact
from yargy.pipelines import morph_pipeline, pipeline
from yargy.predicates import eq, gte, in_, lte, normalized, type

from src.utils import load_lines

seed(41)

INT = type("INT")
COMMA = eq(",")
COLON = eq(":")

COMMA_OR_COLON = or_(COMMA, COLON)


def create_rules_for_age():
    Age_interpretation = fact("Age", ["age"])

    AGE_INT = and_(gte(10), lte(80)).interpretation(Age_interpretation.age.custom(int))

    AGE = rule(AGE_INT, normalized("год")).interpretation(Age_interpretation)

    return AGE


def create_rules_for_sex():
    Sex_interpretation = fact("sex", ["gender"])

    GENDERS = {
        "Женщина": "female",
        "женщина": "female",
        "жен": "female",
        "Мужчина": "male",
        "мужчина": "male",
        "муж": "male",
    }

    SEX = rule(in_(GENDERS)).interpretation(
        Sex_interpretation.gender.custom(GENDERS.get)
    )

    return SEX


def create_rules_for_money():
    Money_interpretation = fact("Money", ["amount", "currency"])

    CURRENCIES = {
        "рублей": "RUB",
        "руб": "RUB",
        "грн": "GRN",
        "бел рублей": "BEL",
        "бел руб": "BEL",
        "RUB": "RUB",
        "EUR": "EUR",
        "KZT": "KZT",
        "USD": "USD",
        "KGS": "KGS",
    }

    CURRENCY = pipeline(CURRENCIES).interpretation(
        Money_interpretation.currency.normalized().custom(CURRENCIES.get)
    )

    AMOUNT = or_(
        rule(INT),
        rule(INT, INT),
        rule(INT, "-", INT),
    )

    MONEY = rule(AMOUNT, CURRENCY)
    return MONEY


def create_rules_for_addr():
    Address_interpretation = fact("Location", ["area", "metro"])

    AREAS = set(load_lines("data/areas.txt"))
    METRO_STATIONS = set(load_lines("data/metro.txt"))

    AREA = morph_pipeline(AREAS).interpretation(Address_interpretation.area)
    METRO_STATIONS = morph_pipeline(METRO_STATIONS)

    metro_symbol = or_(rule(eq("м"), eq(".").optional()), rule(eq("метро")))

    METRO = rule(
        metro_symbol,
        METRO_STATIONS,
    ).interpretation(Address_interpretation.metro)

    METRO_WITHOUT_SYMBOL = rule(
        metro_symbol.optional(),
        METRO_STATIONS,
    ).interpretation(Address_interpretation.metro)

    ADDR = or_(
        rule(AREA, rule(COMMA_OR_COLON.optional(), METRO.repeatable()).optional()),
        rule(
            AREA.optional(),
            rule(COMMA_OR_COLON.optional(), METRO_WITHOUT_SYMBOL.repeatable()),
        ),
    ).interpretation(Address_interpretation)
    return ADDR


def create_rules_for_vac():
    Position_interpretation = fact(
        "Position", ["title", "specialization_subspecialization"]
    )

    SPECIALIZATIONS = set(load_lines("data/specialization.txt"))
    SUBSPECIALIZATIONS = set(load_lines("data/subspecialization.txt"))

    SPECIALIZATION = morph_pipeline(SPECIALIZATIONS).interpretation(
        Position_interpretation.specialization_subspecialization
    )
    SUBSPECIALIZATION = morph_pipeline(SUBSPECIALIZATIONS).interpretation(
        Position_interpretation.specialization_subspecialization
    )

    DESIRED_POSITION_TITLE = morph_pipeline(
        [
            "Желаемая должность",
            "Желаемая должность и зарплата",
            "Желаемая позиция",
            "Желаемая позиция и зарплата",
            "Должность и зарплата",
            "Должность",
            "Позиция и зарплата",
            "Позиция",
        ]
    ).interpretation(Position_interpretation.title)

    VAC = or_(
        rule(DESIRED_POSITION_TITLE, SPECIALIZATION),
        rule(DESIRED_POSITION_TITLE, SUBSPECIALIZATION.repeatable()),
    ).interpretation(Position_interpretation)
    return VAC


def create_rules_for_sch():
    Schedule_interpretation = fact("Schedule", ["title", "schedule"])

    TITLE = morph_pipeline(
        [
            "График работы:",
            "График",
        ]
    ).interpretation(Schedule_interpretation.title)

    TYPES = {
        "полный день": "full",
        "неполный день": "part",
        "Полный рабочий день": "full",
        "Неполный рабочий день": "part",
        "Частичный рабочий день": "part",
        "сменный график": "part",
        "вахтовый метод": "vahta",
        "гибкий график": "flex",
        "удаленная работа": "remote",
        "стажировка": "intern",
        "проектная работа": "by_project",
    }

    TYPE = morph_pipeline(TYPES).interpretation(
        Schedule_interpretation.schedule.normalized().custom(TYPES.get)
    )

    TYPES = rule(TYPE, rule(COMMA, TYPE).optional().repeatable()).interpretation(
        Schedule_interpretation.schedule
    )

    SCH = rule(TITLE.optional(), TYPES).interpretation(Schedule_interpretation)

    return SCH


def create_yargy_pipeline():
    entities_parsers = {
        "SEX": Parser(create_rules_for_sex()),
        "AGE": Parser(create_rules_for_age()),
        "ADDR": Parser(create_rules_for_addr()),
        "SAL": Parser(create_rules_for_money()),
        "VAC": Parser(create_rules_for_vac()),
        "SCH": Parser(create_rules_for_sch()),
    }

    return entities_parsers
