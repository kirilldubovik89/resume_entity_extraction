weights = {
    "AGE": 198,
    "SEX": 100,
    "VAC": 250,
    "SAL": 365,
    "SCH": 370,
    "ADDR": 375,
    "EDU": 1846,
    "LEXP": 218,
    "LPST": 212,
    "LORG": 242,
}


def get_labels_for_f1(text, label):
    text = text.replace('B-', '')
    text = text.replace('I-', '')
    tags = ['VAC', 'SAL', 'SCH', 'EDU', 'LORG', 'LPST', 'LEXP', 'ADDR', 'AGE', 'SEX']
    for tag in tags:
        if tag != label:
            text = text.replace(tag, 'O')
    return text


def compute_f1(true_labels, pred_labels, label) -> float:
    """
    Compute the F1 score for a named entity recognition task.
    """
    true_entities = true_labels.split()
    pred_entities = pred_labels.split()

    tp, tn, fp, fn = 0, 0, 0, 0

    for i in range(len(true_entities)):
        if pred_entities[i] == label:
            if pred_entities[i] == true_entities[i]:
                tp += 1
            else:
                fp += 1
        if pred_entities[i] == 'O':
            if pred_entities[i] == true_entities[i]:
                tn += 1
            else:
                fn += 1

    if label not in true_entities and label not in pred_entities:
        return 1

    if tp == 0:
        return 0

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = 2 * precision * recall / (precision + recall)

    return f1