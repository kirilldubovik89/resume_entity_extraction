import json

from ipymarkup import show_span_ascii_markup as show_markup


def normalize_amount(value):
    return int(value.replace(" ", ""))


def parse_all_entities(text, entities_parsers: dict):
    entities = []
    for entity_name, entity_parser in entities_parsers.items():
        entity = [
            (entity_name, list(match_entity.span)[0], list(match_entity.span)[1])
            for match_entity in entity_parser.findall(text)
        ]
        entities.extend(entity)
    entities = sorted(entities, key=lambda x: x[1])
    return entities


def load_lines(path):
    with open(path) as file:
        for line in file:
            yield line.rstrip("\n")


def show_json(data):
    print(json.dumps(data, indent=2, ensure_ascii=False))


def show_matches(rule, *lines, Parser):
    parser = Parser(rule)
    for line in lines:
        matches = parser.findall(line)
        spans = [_.span for _ in matches]
        show_markup(line, spans)
