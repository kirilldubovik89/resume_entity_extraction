# Resume Entity Extraction
A system for extracting named entities from resumes

## Installation
``` shell
# clone repo:
git clone https://gitlab.com/kirilldubovik89/resume_entity_extraction.git

# go to the folder:
cd resume-entity-extraction

# create virtualenv:
poetry install

# activate virtualenv:
poetry shell

```

## Introduction
The task of extracting NER from a resume is important for the work of the "rabota.ru" service, because it is based on the actual business processes inside the company.

## Model Description
In this paper, the first step for the NER problem is to apply the yargy parser.
This library is part of the larger Natasha project and, in fact, the basis of the natasha library. Yargy-parser is an analog of Yandex Tomite-parser for Python. Entity extraction rules are described using context-free grammars and dictionaries to extract structured information from Russian texts. The rules consist of predicates. Yargy-parser has many built-in predicates, such as
- is_capitalized - the word starts with a capital letter,
- is_single - word in singular, gram (X), where X is a gramma.

## Dataset
The dataset for this task was collected independently from open data of "rabota.ru" service. The stages of document preprocessing are presented in the project repository.
This section presents the statistics of the dataset

| Indicator name                                                        | Value      |
|-----------------------------------------------------------------------|------------|
| Number of resumes                                                     | 590        |
| Number of tokens                                                      | 161,043    |
| Number of unique tokens, incl. words, numbers and punctuation marks   | 15,943     |
| Vocabulary size - number of unique token words reduced to normal form | 7,633      |

From the collected 590 resumes, 100 resumes were selected and manually labeled to assess the quality of the implemented NER allocation models. 
The entities you are looking for include:
- age (label AGE);
- sex (label SEX);
- anticipated vacancy (label VAC);
- expected salary (label SAL);
- desired work schedule (label SCH);
- job address (label ADDR)

## Metrics
To assess the quality of the models it is accepted the weighted-averaged F1.

## Results
The results achieved are shown in the table below.

| Indicator   | Value      |Weight  |
|-------------|------------|--------|
| F1 SEX      | 1.0        | 0.060  |
| F1 AGE      | 0.979      | 0.119  |
| F1 SAL      | 0.856      | 0.220  |
| F1 ADDR     | 0.275      | 0.226  |
| F1 VAC      | 0.283      | 0.151  |
| F1 SCH      | 0.944      | 0.223  |
| F1 weighted | 0.681      | 1.0    |


